import React, { useState } from 'react';
import { Input, Button } from '@mui/material';
import axios from 'axios';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

export default function CsvReader() {
  const [csvFile, setCsvFile] = useState();
  const [csvArray, setCsvArray] = useState([]);
  
  const onFileUpload = (e) => {
    console.log(e.target.files[0])
    setCsvFile(e.target.files[0])
  }

  const processCsv = (str) => {
    let headers = str.slice(0, str.indexOf('\n')).split(',');
    
    headers = headers.map((item) => {
      console.log(item.replace(/(\r\n|\n|\r)/gm, ""))
      return item.replace(/(\r\n|\n|\r)/gm, "")
    })
    
    const rows = str.slice(str.indexOf('\n')+1).split('\n');
    // console.log(rows)

    const newArray = rows.map((row) => {
      const item = row.split(',');
      const eachItem = headers.reduce((obj, header, i) => {
        // console.log(obj, header)
        obj[header] = item[i]
        // console.log(obj)
        return obj
      }, {})
      return eachItem
    })
    // console.log(newArray)
    setCsvArray(newArray)

    // Delete Data
    axios.delete('/del-data').then(res => {
      console.log(res)
    })
    .catch(err => {
      console.log(err)
    })

    axios.post('/insert-data-from-csv', newArray)
    .then((res) => {
      console.log(res)
    }).catch((err) => {
      console.log(err)
    })
  }

  function sortByRank(col) {
    let sortedItems = csvArray.sort((a, b) => {
      if ( a[col] < b[col] ){
        return 1;
      }
      if ( a[col] > b[col] ){
        return -1;
      }
      return 0;
    })
    setCsvArray([...sortedItems])
  }

  const submit = (e) => {
    console.log('asd')
    if (!csvFile) {
      console.log('nothing')
      return
    };
    const file = csvFile;
    const reader = new FileReader();
    
    reader.onload = function(e) {
      const text = e.target.result;
      // console.log(text)
      processCsv(text)
    }

    reader.readAsText(file)
  }

  console.log(csvArray)

  const search = (e) => {

    function filterItems(arr, query) {
      return arr.filter(function(el) {
        return el.Year.toLowerCase().indexOf(query.toLowerCase()) !== -1
      })
    }

    setCsvArray([...filterItems(csvArray, e.target.value)])

  }

  return (
    <div>
      <Box sx={{mb: 2, mx: "auto", width: "416px"}}>
        <form>
          <Input accept=".csv" id="file-upload-button" multiple type="file" onChange={onFileUpload} />
          <Button style={{marginLeft: '1rem'}} component="span" variant="contained" onClick={submit}>Upload</Button>
        </form>
      </Box>

      <Box sx={{mb: 2, mx: "auto", width: 200}}>
        <Input placeholder="Placeholder" onChange={search} />
      </Box>

      {csvArray.length > 0 ? 
        <div>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Year</TableCell>
                  <TableCell align="right"><Button onClick={() => sortByRank('Rank')}>Rank</Button></TableCell>
                  <TableCell align="right"><Button onClick={() => sortByRank('Recipient')}>Recipient</Button></TableCell>
                  <TableCell align="right"><Button onClick={() => sortByRank('Country')}>Country</Button></TableCell>
                  <TableCell align="right"><Button onClick={() => sortByRank('Career')}>Career</Button></TableCell>
                  <TableCell align="right"><Button onClick={() => sortByRank('Tied')}>Tied</Button></TableCell>
                  <TableCell align="right"><Button onClick={() => sortByRank('Title')}>Title</Button></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {csvArray.map((row, i) => (
                  <TableRow key={i}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.Year}
                    </TableCell>
                    <TableCell align="right">{row.Rank}</TableCell>
                    <TableCell align="right">{row.Recipient}</TableCell>
                    <TableCell align="right">{row.Country}</TableCell>
                    <TableCell align="right">{row.Career}</TableCell>
                    <TableCell align="right">{row.Tied}</TableCell>
                    <TableCell align="right">{row.Title}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div> : <div></div>}

    </div>
  )
}