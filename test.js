let fruits = [{
  name: 'Jason',
  lastName: 'Flores'
}, {
  name: 'qwe',
  lastName: 'ssss'
}]

function filterItems(arr, query) {
  return arr.filter(function(el) {
    return el.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
  })
}

console.log(filterItems(fruits, 'son'))  // ['apple', 'grapes']